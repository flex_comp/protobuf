package protobuf

import "errors"

var (
	ErrSumInvalid   = errors.New("message sum is invalid")
	ErrUnknownProto = errors.New("unknown proto")
)
