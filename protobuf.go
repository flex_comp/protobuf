package protobuf

import (
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/conf"
	"gitlab.com/flex_comp/util"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
	"hash/adler32"
	"strconv"
)

var (
	pb *Protobuf
)

func init() {
	pb = &Protobuf{
		sortNameIndex: make(map[string]protoreflect.MessageType),
		fullNameIndex: make(map[string]protoreflect.MessageType),
		salt:          []byte{},
	}

	_ = comp.RegComp(pb)
}

type Protobuf struct {
	sortNameIndex map[string]protoreflect.MessageType
	fullNameIndex map[string]protoreflect.MessageType
	salt          []byte
}

func (p *Protobuf) Init(execArgs map[string]interface{}, _ ...interface{}) error {
	p.salt = []byte(util.ToString(conf.Get("protobuf.salt")))
	messages := util.ToSlice(execArgs["protos"])

	for _, v := range messages {
		m := v.(proto.Message)
		p.sortNameIndex[string(proto.MessageName(m).Name())] = m.ProtoReflect().Type()
		p.fullNameIndex[string(proto.MessageName(m))] = m.ProtoReflect().Type()
	}
	return nil
}

func (p *Protobuf) Start(_ ...interface{}) error {
	return nil
}

func (p *Protobuf) UnInit() {
}

func (p *Protobuf) Name() string {
	return "protobuf"
}

func ProtoType(fullName string) protoreflect.MessageType {
	return pb.ProtoType(fullName)
}

func (p *Protobuf) ProtoType(fullName string) protoreflect.MessageType {
	ty := p.fullNameIndex[fullName]
	return ty
}

func MarshalReq(req *Request, msg proto.Message) (bs []byte, err error) {
	return pb.MarshalReq(req, msg)
}

func (p *Protobuf) MarshalReq(req *Request, msg proto.Message) (bs []byte, err error) {
	data, err := proto.Marshal(msg)
	if err != nil {
		return nil, err
	}

	req.Head = string(proto.MessageName(msg).Name())
	req.Data = data

	a := adler32.New()
	_, err = a.Write(req.Data)
	if err != nil {
		return
	}

	_, err = a.Write(p.salt)
	if err != nil {
		return
	}

	_, err = a.Write([]byte(req.ClientID))
	if err != nil {
		return
	}

	_, err = a.Write([]byte(strconv.FormatInt(int64(req.GameServerID), 10)))
	if err != nil {
		return
	}

	_, err = a.Write([]byte(strconv.FormatInt(int64(req.CmdNo), 10)))
	if err != nil {
		return
	}

	req.Sum = a.Sum32()
	return proto.Marshal(req)
}

func UnMarshalReq(data []byte) (req *Request, msg proto.Message, err error) {
	return pb.UnMarshalReq(data)
}

func (p *Protobuf) UnMarshalReq(data []byte) (req *Request, msg proto.Message, err error) {
	req = new(Request)
	if e := proto.Unmarshal(data, req); e != nil {
		err = e
		return
	}

	a := adler32.New()
	_, err = a.Write(req.Data)
	if err != nil {
		return
	}

	_, err = a.Write(p.salt)
	if err != nil {
		return
	}

	_, err = a.Write([]byte(req.ClientID))
	if err != nil {
		return
	}

	_, err = a.Write([]byte(strconv.FormatInt(int64(req.GameServerID), 10)))
	if err != nil {
		return
	}

	_, err = a.Write([]byte(strconv.FormatInt(int64(req.CmdNo), 10)))
	if err != nil {
		return
	}

	// 校验摘要
	if a.Sum32() != req.Sum {
		err = ErrSumInvalid
		return
	}

	// 解业务包
	ty, ok := p.sortNameIndex[req.Head]
	if !ok {
		err = ErrUnknownProto
		return
	}

	msg = ty.New().Interface()
	err = proto.Unmarshal(req.Data, msg)
	return
}

func MarshalRsp(rsp *Response, msg proto.Message) ([]byte, error) {
	return pb.MarshalRsp(rsp, msg)
}

func (p *Protobuf) MarshalRsp(rsp *Response, msg proto.Message) ([]byte, error) {
	data, err := proto.Marshal(msg)
	if err != nil {
		return nil, err
	}

	rsp.Head = string(proto.MessageName(msg).Name())
	rsp.Data = data

	return proto.Marshal(rsp)
}

func UnMarshalRsp(data []byte) (rsp *Response, msg proto.Message, err error) {
	return pb.UnMarshalRsp(data)
}

func (p *Protobuf) UnMarshalRsp(data []byte) (rsp *Response, msg proto.Message, err error) {
	rsp = new(Response)
	if e := proto.Unmarshal(data, rsp); e != nil {
		err = e
		return
	}

	// 解业务包
	ty, ok := p.sortNameIndex[rsp.Head]
	if !ok {
		err = ErrUnknownProto
		return
	}

	msg = ty.New().Interface()
	err = proto.Unmarshal(rsp.Data, msg)
	return
}

func MarshalRspRaw(rsp *Response) ([]byte, error) {
	return pb.MarshalRspRaw(rsp)
}

func (p *Protobuf) MarshalRspRaw(rsp *Response) ([]byte, error) {
	return proto.Marshal(rsp)
}

func UnMarshalRspRaw(data []byte) (rsp *Response, err error) {
	return pb.UnMarshalRspRaw(data)
}

func (p *Protobuf) UnMarshalRspRaw(data []byte) (rsp *Response, err error) {
	rsp = new(Response)
	err = proto.Unmarshal(data, rsp)
	return
}
